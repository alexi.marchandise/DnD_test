# DnD_test

## Description
DnD_test is a command-line based symfony app that allows users to pull a csv file from a url and get its content as a return, in specified format.
It is a little test **Agence Dn'D** wanted me to do before meeting me.

## Commands
`php bin/console app:display-data [url] [format]`
- **url** represents the file url.
- **format** represents the return format.

### formats
- default format is a **command-line table**.
- **json** is another supported format.
