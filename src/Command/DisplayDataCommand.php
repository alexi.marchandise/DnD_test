<?php 

namespace App\Command;

use App\Model\DataModel;
use App\Service\RendererService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class DisplayDataCommand extends Command {
    /** @var string $defaultName the name of the command (the part after "bin/console") */
    protected static $defaultName = 'app:display-data';

    /** @var HttpClientInterface $client */
    private $client;

    /** @var Serializer $serializer */
    private $serializer;

    public function __construct(HttpClientInterface $client)
    {
        parent::__construct();
        $this->client = $client;
        $encoders = [new JsonEncoder(), new CsvEncoder()];
        $normalizers = [new ObjectNormalizer(), new ArrayDenormalizer()];
        $this->serializer = new Serializer($normalizers, $encoders);
    }

    protected function configure()
    {
        $this
            // configure arguments
            ->addArgument('url', InputArgument::REQUIRED, 'The url of the csv file.')
            ->addArgument('format', InputArgument::OPTIONAL, 'The format wanted (table or json)', 'table')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $renderer = new RendererService($input, $output);
        //Retrieve data from url
        $data = $this->getData($input->getArgument('url'));

        //Render data...
        if($input->getArgument('format') === 'json') {
            //...as a json if requested
            $json = $this->serializer->serialize($data, 'json');
            $renderer->renderJson($json);
        } else {
            //...as a table by default
            $renderer->renderTable($data);
        }

        return Command::SUCCESS;
    }

    //Retrieve deserialized data
    protected function getData(String $url)
    {
        //retrive data
        $response = $this->client->request(
            'GET',
            $url
        );

        //return the data deserialized
        return $this->serializer->deserialize($response->getContent(), DataModel::class.'[]', 'csv', ['csv_delimiter' => ';']);
    }
}
