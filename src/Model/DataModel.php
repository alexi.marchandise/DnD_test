<?php

namespace App\Model;

use DateTime;

use Symfony\Component\String\UnicodeString;

class DataModel {
    private $sku;
    private $title;
    private $slug;
    private $isEnabled;
    private $price;
    private $description;
    private $createdAt;

    //return attributes names of this object
    public function getAttributes()
    {
        return array_keys(get_object_vars($this));
    }

    //Return this properties values as array
    public function getValues()
    {
        $attributes = $this->getAttributes();
        $values = [];

        foreach ($attributes as $key) {
            $values[] = $this->{'get'.$key}();
        }

        return $values;
    }

    /**
     * Get the value of sku
     */ 
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Set the value of sku
     *
     * @return  self
     */ 
    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Get the value of title
     */ 
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of title
     *
     * @return  self
     */ 
    public function setTitle($title)
    {
        $this->title = $title;
        $this->setSlug($this->getTitle());

        return $this;
    }

    /**
     * Get the value of isEnabled
     */ 
    public function getIsEnabled()
    {
        return $this->isEnabled;
    }

    /**
     * Set the value of isEnabled
     *
     * @return  self
     */ 
    public function setIsEnabled($isEnabled)
    {
        $this->isEnabled = $isEnabled === '1'?'Enable':'Disable';

        return $this;
    }

    /**
     * Get the value of price
     */ 
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set the value of price
     *
     * @return  self
     */ 
    public function setPrice($price)
    {
        $price = (float)$price;
        $price = number_format($price, 2, ',', ' ');
        $this->price = (string) $price;

        return $this;
    }

    /**
     * Set the value of currency
     *
     * @return  self
     */ 
    public function setCurrency($currency)
    {
        $this->price .= $currency;

        return $this;
    }

    /**
     * Get the value of description
     */ 
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @return  self
     */ 
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of createdAt
     */ 
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set the value of createdAt
     *
     * @return  self
     */ 
    public function setCreatedAt($createdAt)
    {
        $createdAt = (new DateTime($createdAt))->format('l, d-M-Y H:i:s e');

        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get the value of slug
     */ 
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set the value of slug
     *
     * @return  self
     */ 
    public function setSlug($slug)
    {
        $slug = (new UnicodeString($slug))
            ->lower()
            ->replaceMatches('/[^a-zA-Z1-9 ]/', '-')
            ->replace(' ', '_')
        ;

        $this->slug = $slug->toString();

        return $this;
    }
}
