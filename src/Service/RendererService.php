<?php

namespace App\Service;

use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RendererService {
    private $io;

    public function __construct(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    //renders data as a table in console
    public function renderTable($data) {

        $headersArray = $data[0]->getAttributes();
        $arrayRows = [];

        foreach ($data as $row) {
            $arrayRows[] = $row->getValues();
        }

        $this->io->table($headersArray, $arrayRows);
    }

    //render data as plain json in console
    public function renderJson($data) {
        $this->io->text($data);
        $this->io->newLine();
    }
}
